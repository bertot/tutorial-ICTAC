Require Import Arith List.

Fixpoint my_filter {T : Type} (p : T -> bool)
    (l : list T) : list T :=
match l with
  nil => nil
| a :: l1 => if p a then a :: my_filter p l1 else my_filter p l1
end.

Lemma In_my_filter T (p : T -> bool) x l :
  In x (my_filter p l) <-> In x l /\ p x = true.
Proof.
induction l as [ | a l IHl].
  simpl.
  split.
    intros absurdity; elim absurdity.
  intros [absurdity _]; exact absurdity.
simpl.
split.
  case_eq (p a); intros pa_eq; simpl.
    intros [a_is_x |  x_in].
      split.
        left.
        assumption.
      rewrite <- a_is_x.
      assumption.
    rewrite IHl in x_in.
    tauto.
  rewrite IHl; tauto.
intros [[a_is_x | x_in] pxtrue].
  rewrite a_is_x.
  rewrite pxtrue.
  simpl.
  left.
  easy.
case (p a).
  simpl.
  right.
  rewrite IHl.
  split; assumption.
tauto.
Qed.
