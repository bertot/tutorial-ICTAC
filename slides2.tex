\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{color}
\newcommand{\coqor}{{\tt\char'134/}}
\newcommand{\coqand}{{\tt/\char'134}}
\newcommand{\coqnot}{\mbox{\~{}}}

\setbeamertemplate{footline}[frame number]
\title{Logic and specification in Coq}
\author{Yves Bertot}
\date{October 2018}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Starting from tests}

\begin{itemize} 
\item Tests rely on two components
\begin{itemize}
\item A piece of code to generate test cases
\item A piece of code to verify correct behavior on all cases
\end{itemize}
\item In our approach, we use logic
\begin{itemize}
\item to describe what are all possible inputs
\item to express what is the correct behavior
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Example on the filter function}
\begin{itemize}
\item What do you expect from the filter function?
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Filter function specification}
\begin{itemize}
\item The output of the filter function must only contain values satisfying
the boolean property
\item All values satisfying the boolean property should be taken
\pause
\item The multiplicity of values is preserved
\item The order of values is preserved
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Expressing logical property}


\begin{itemize}
\item Difficulty of Coq: logical values are not boolean

\item In a sense, {\tt bool} is restricted to logical statements that
can be decided
\begin{itemize}
\item This is sometimes circumvented by axioms
\end{itemize}
\item A new type {\tt Prop} is used for logical propositions

\item For {\tt T} a type and {\tt a b~:~T}, {\tt a = b~:~Prop}

\item {\tt Prop} also has connectives {\tt and} (\coqand{}), {\tt or}
(\coqor{}), {\tt not} (\coqnot{}), implication is written {\tt ->}

\item Universal quantification is written {\tt forall x : T, P x}
\item Existential quantification {\tt exists x : T, P x}
\item A boolean value \(v\) can be mapped to a proposition by writing
{\tt \(v\) = true}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Propositions for lists and numbers}
\begin{itemize}
\item {\tt In x l} is defined by a recursive computation that
yields a proposition based on {\tt =} and \coqor{}
\item Numbers have {\tt <=}, {\tt <}
\end{itemize}
\begin{alltt}
Compute In (2 + 3) (3 :: 5 :: 8 :: nil).
\textcolor{blue}{= 3 = 5 \coqor{} 5 = 5 \coqor{} 8 = 5 \coqor{} False : Prop}
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Specifications}
\begin{alltt}
Definition multiple_of div n :=
   exists k, n = k * div.

Definition prime n :=
  1 < n \coqand{}
  forall x, 1 < x < n -> \coqnot{}multiple_of x n.

forall (A : Type) (f : A -> bool) (x : A) (l : list A),
  In x (filter f l) <-> In x l \coqand{} f x = true
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{specifications based on tests}
\begin{itemize}
\item Write your function: {\tt f~:~A -> B}
\item Write extra functions to verify that the output is correct,
{\tt verif~:~B -> bool}
\item Express a universal statement
{\tt forall x~:A, verif (f x) = true}
\item Being able to prove such a statement is equivalent to exhaustive testing.
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Performing proofs}
\begin{itemize}
\item Interactive loop
\item Start with {\tt Lemma {\sl name} : {\sl statement}.  Proof.}
\item Then apply commands that decompose the goal.
\item {\tt intros}, {\tt split}, {\tt exists}, {\tt left}, {\tt right},
{\tt auto}, {\tt rewrite}.
\item use {\tt apply} with existing theorems.
\item Special case for {\tt In}: {\tt simpl} or {\tt compute}
\item When finished, save using {\tt Qed.}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example on slide}
\begin{alltt}
Lemma exinlist : In 3 (2 :: 3 :: 5 :: 8 :: nil).
Proof.
compute.
\textcolor{blue}{1 subgoal
  
  ============================
  2 = 3 \coqor{} 3 = 3 \coqor{} 5 = 3 \coqor{} 8 = 3 \coqor{} False}
right.
\textcolor{blue}{  3 = 3 \coqor{} 5 = 3 \coqor{} 8 = 3 \coqor{} False}
left.
\textcolor{blue}{ 3 = 3}
auto.
\textcolor{blue}{No more subgoals.}
Qed.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Difficulties with logical reasoning}
\begin{itemize}
\item Beginners have troubles with making the difference between
various combinations of conjunction (``and'') and implication
\item Reasoning about negation and false premises is especially difficult
\item The logic of Coq is ``intuitionistic'': proofs most often have to be
constructive
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Using libraries}
\begin{itemize}
\item Coq comes with existing libraries
\item Existing functions have theorems about them
\item Important command : {\tt Search}
\end{itemize}
\begin{alltt}
Search filter.
\textcolor{blue}{filter_In:
  forall (A : Type) (f : A -> bool) (x : A) (l : list A),
  In x (filter f l) <-> In x l /\ f x = true}
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Examples of powerful libraries}
\begin{itemize}
\item Mathematical Components
\begin{itemize}
\item Designed for the 4-color theorem, the odd order theorem (Feit-Thompson)
\item Used for elliptic curves, reasoning about robots, combinatorics,
transcendance proofs
\end{itemize}
\item Coquelicot
\begin{itemize}
\item Real analysis: limits, derivatives, integrals, mathematical functions
\item Used for numerical computations (wave function, mathematical constants)
\end{itemize}
\item VST
\begin{itemize}
\item Reasoning about programs in C (in connection with Compcert)
\item Used for pointer data structures, security proofs
\end{itemize}
\end{itemize}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
