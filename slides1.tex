\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{color}
\setbeamertemplate{footline}[frame number]
\title{Programming in Coq}
\author{Yves Bertot}
\date{October 2018}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Why use Formal verification tools}

\begin{itemize} 
\item Write programs with less bugs

\item Document the programs with logical statements

\item Verify the logical statements with the computer

\item Model and verify existing programs or systems
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Example of program with less bugs}


\begin{itemize}
\item Compcert C compiler

\item Use a formal description of the C programming language and assembly languages

\item Construct a formal description of a compiler from C to assembly

\item {\bf Prove} that the compiler respects the semantics of programs

\item Independently tested by the C-smith tool

\item More information on \url{http://compcert.inria.fr/motivations.html}
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Compiler correctness statement}
Taken from CompCert 3.4, file {\tt driver/Compiler.v}
\begin{small}
\begin{alltt}
Theorem transf_c_program_correct:
  forall p tp,
  transf_c_program p = OK tp ->
  backward_simulation (Csem.semantics p) (Asm.semantics tp).
Proof.
  intros.
  apply c_semantic_preservation.
  apply transf_c_program_match; auto.
Qed.
\end{alltt}
\end{small}
\end{frame}

\begin{frame}
\frametitle{Ambitious projects}


\begin{itemize}
\item End-to-end verification of large systems

\item Formal verification brings composability

\item Complex inner parts can be factored out

\item You only need to understand the definition and the top statement

\item The odd-order theorem in group theory (Feit-Thompson) is an example
\begin{itemize}
\item The definitions and the statements fit in two pages
\end{itemize}

\end{itemize}

\end{frame}
\begin{frame}
\frametitle{Let's start easy}
Two ways to develop software in Coq
\begin{itemize}
\item Describe algorithms inside Coq, Execute outside
\begin{itemize}
\item Stronger programming tools
\item Lighter runtime environment
\end{itemize}
\item Do everything inside Coq
\begin{itemize}
\item Simpler programming language
\item Use Coq as an interpreter
\item Instant feedback
\end{itemize}
\item We will mostly show the latter
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Basic data structures}
\begin{itemize}
\item numbers {\tt 1}, {\tt 42}, {\tt 1024}
\item boolean values {\tt true}, {\tt false}
\item pairs {\tt (1, true)}
\item lists of things {\tt 1 :: 2 :: 3 :: 4 :: nil}
\item functions {\tt fun x => x}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{More about functions}
\begin{itemize}
\item binary operations on numbers {\tt +}, {\tt *}, {\tt /} {\tt mod}, {\tt -}
\item boolean relations on numbers {\tt <?}, {\tt =?}, {\tt <=?}
\item boolean operations on boolean values {\tt \&\&}, {\tt ||}
\item boolean negation {\tt negb}
\item test on boolean value {\tt if  then  else}
\item projections on pairs {\tt fst}, {\tt snd}
\item more complex programming structure for lists (to be given later)
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Defining and using your own functions}
\begin{itemize}
\item Give a name to a value : {\tt Definition name := value.}
\begin{itemize}
\item Give a name to a function :\\ {\tt Definition fname := fun x => x}.
\item Alternative : {\tt Definition fname x := x.}
\end{itemize}
\item Use a function: write the name before the argument\\
  write f (fname 1)
\begin{itemize}
\item parentheses not always needed
\end{itemize}
\item Check your own steps using the {\tt Check} command.
\item Compute your examples using the {\tt Compute} command.
\item Know what is defined using the {\tt Print}.
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Examples}
\begin{alltt}
Require Import Arith Bool List.

Definition add2 x := x + 2.

Check add2 3.
\textcolor{blue}{add2 3 : nat}

Compute add2 3.
\textcolor{blue}{= 5 : nat}

Definition twice (f : nat -> nat) (x : nat) := f (f x).

Compute twice (twice add2) 1.
\textcolor{blue}{= 9 : nat}
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Comments on the examples}
\begin{itemize}
\item {\tt twice} is a function with two arguments
\begin{itemize}
\item the syntax is really different from {\tt C, java, etc.}
\end{itemize}
\item parentheses are needed around {\tt f x} in the definition of twice
\item {\bf No parentheses} around the two arguments in the use of {\tt twice}
\item {\tt twice} can also be used with only one argument
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Functions about data-structures}
\begin{itemize}
\item components of a pair : {\tt fst}, {\tt snd}
\item Fetching elements of a list
\end{itemize}
\begin{alltt}
match l with
| a :: l1 => f a l1
| nil => v
end
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Programming with lists}
\begin{alltt}
Definition headplus1 (l : list nat) :=
  match l with
    a :: l1 => a + 1
  | nil => 0
  end.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Recursive programming with lists}
\begin{itemize}
\item Lists can be arbitrary long
\item A list has a sub-component that is itself a list
\item A recursive program can call itself on that sub-component
\end{itemize}
\begin{small}
\begin{alltt}
Fixpoint grow_nat (l : list nat) :=
match l with 
  nil => nil
| a :: l1 => 2 * a :: 2 * a + 1 :: grow_nat l1
end.

Fixpoint my_filter \{T : Type\} (p : T -> bool)
    (l : list T) : list T :=
match l with
  nil => nil
| a :: l1 => if p a then a :: my_filter p l1 else my_filter p l1
end.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}
\frametitle{Comments on list programming}
\begin{itemize}
\item Lists and pairs are {\em polymorphic} data structures
\item You don't need to know the type of elements for many operations
\item You can choose for the type argument to be implicit.
\item No undefined behavior: all functions must cover the case where the
argument is empty
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Making your own data type}
\begin{itemize}
\item Lists are an example of data type with two cases, and one of the
cases has two sub-components
\item You can make your own choice.
\item Example drawn from an example available in {\tt coq-contribs},
{\tt semantics}
\end{itemize}
\begin{alltt}
Inductive aexpr0 : Type := 
  avar (s : string) 
| anum (n :Z) 
| aplus (a1 a2:aexpr0).

Inductive bexpr0 : Type :=  blt (_ _ : aexpr0).

Inductive instr0 : Type :=
  assign (_ : string) (_ : aexpr0)
| sequence (_ _: instr0)
| while (_ :bexpr0)(_ : instr0)
| skip.

\end{alltt}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
