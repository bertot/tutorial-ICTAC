\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{alltt}
\usepackage{color}
\setbeamertemplate{footline}[frame number]
\title{General datatypes and algorithms}
\author{Yves Bertot}
\date{October 2018}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Objectives}
\begin{itemize}
\item Explore more varied data-types
\begin{itemize}
\item More cases and more components
\item With dependency between the components
\end{itemize}
\item Explore more general recursion
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Trees}
\begin{itemize}
\item We already saw how to represent a programming language
\begin{enumerate}
\item As many cases as there kinds of instructions
\item Each instruction may have several sub-components
\end{enumerate}
\item As a smaller example we show a data-type of binary trees
\end{itemize}
\begin{alltt}
Inductive btree (T : Type) : Type :=
  Leaf | Node (val : T) (t1 t2 : btree T).
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{recursive programming on binary trees}
\begin{alltt}
Fixpoint rev_tree \{T : Type\} (t : btree T) : btree T :=
match t with
| Leaf => Leaf
| Node x t1 t2 => Node x (rev_tree t1) (rev_tree t2)
end.

Arguments Leaf \{T\}.
Arguments Node \{T\}.

\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{recursive programming on binary trees (2)}
\begin{alltt}
Fixpoint count \{T : Type\} (p : T -> bool)
   (t : btree T) : nat :=
match t with
| Leaf => 0
| Note x t1 t2 =>
  (if p x then 1 else 0) + (count p t1 + count p t2)
end.

Definition size \{T\} (t : btree T) :=
  count (fun x => true) t.
\end{alltt}
\begin{itemize}
\item recursive calls are allowed on both sub-trees
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Proof by induction on trees}
\begin{itemize}
\item The same as for induction on natural numbers or lists
\item Now there are two induction hypotheses
\item Other kinds of ``smaller'' trees not easily handled
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example of a proof by induction}
\begin{small}
\begin{alltt}
Lemma count_rev_tree \{T\} (p : T -> bool) t :
   count p (rev_tree t) = count p t.
Proof.
induction t as [ | a t1 IH1 t2 IH2].
\textcolor{blue}{2 subgoals
count p (rev_tree Leaf) = count p Leaf}
easy.
\textcolor{blue}{count (rev_tree (Node a t1 t2)) = count p (Node a t1 t2)}
simpl.
\textcolor{blue}{(if p a then 1 else 0) + 
         (count p (rev_tree t2) + count p (rev_tree t1)) =
  (if p a then 1 else 0) + (count p t1 + count p t2)}
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example of a proof by induction (2)}
\begin{small}
\begin{alltt}
rewrite IH1.
\textcolor{blue}{  (if p a then 1 else 0) + 
         (count p (rev_tree t2) + count p t1) =
  (if p a then 1 else 0) + (count p t1 + count p t2)}
rewrite IH2.
\textcolor{blue}{  (if p a then 1 else 0) + (count p t2 + count p t1) =
  (if p a then 1 else 0) + (count p t1 + count p t2)}
rewrite (Nat.add_comm (count p t2)).
\textcolor{blue}{  (if p a then 1 else 0) + (count p t1 + count p t2) =
  (if p a then 1 else 0) + (count p t1 + count p t2)}
easy.
Qed.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Induction on the size of trees}
\begin{small}
\begin{alltt}
Definition btree_size_ind
   forall P : btree T -> Prop, 
   (forall t, (forall t', size t' < size t -> P t') -> P t) ->
   forall t, P t
  :=
  well_founded_induction
   (wf_inverse_image _ _ _ size PeanoNat.Nat.lt_wf_0).
\end{alltt}
\end{small}
\begin{itemize}
\item Now only one induction hypothesis
\item But it can be used for any tree, even with different values
\item The same pattern can be used for programming
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Preparatory lemmas for {\tt bree\_size\_ind}}
\begin{small}
\begin{alltt}
Lemma size1 {T} (a : T) t1 t2 : size t1 < size (Node a t1 t2).
Proof.
unfold size; simpl.
unfold lt; apply Peano.le_n_S, Nat.le_add_r.
Qed.

Lemma size2 {T} (a : T) t1 t2 : size t2 < size (Node a t1 t2).
Proof.
unfold size; simpl.
unfold lt; apply Peano.le_n_S; rewrite Nat.add_comm; apply Nat.le_add_r.
Qed.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{A proof using {\tt btree\_size\_ind}}
\begin{small}
\begin{alltt}
Lemma redo_count_rev_tree {T} (p : T -> bool) t :
  count p (rev_tree t) = count p t.
Proof.
induction t as [t IH] using btree_size_ind.
destruct t as [ | a t1 t2].
  easy.
simpl.
rewrite IH.
  rewrite IH.
    rewrite (Nat.add_comm (count p t2)).
    easy.
  apply size1.
apply size2.
Qed.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}
\frametitle{Dependent components and certified data}
\begin{itemize}
\item In pairs, the two components are independent
\item Extension: sigma-types where the second components depends on the first
one
\item Example: bounded integers \((n, h)\) where \(h\) is a proof that \(n\) is
within bounds
\item You need some for of ``proof irrelevance''
\item Used extensively in the {\em Mathematical Components} library
\item Recursive programming on bounded integers is slightly more complex
\end{itemize}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
