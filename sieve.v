From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Fixpoint sieve inputs :=
  match inputs with
  | a :: l' => a :: [seq x <- sieve l' | ~~ (a %| x)]
  | [::] => [::]
  end.

Lemma sieve_keep l1 n l2 :
  prime n -> all (ltn 1) l1 -> n \in sieve (l1 ++ n :: l2).
Proof.
move=> prn; elim: l1 => [ | a l1 IH] /=.
  by rewrite inE eqxx.
rewrite inE mem_filter.
have [/eqP -> | nna] := boolP (n == a) => //= /andP[] agt1 /IH ->.
rewrite andbT; apply/negP=> adiv.
move/primeP: prn => [] ngt1 /(_ _ adiv).
move: agt1; rewrite ?(eq_sym a) ltn_neqAle (negbTE nna).
by move=> /andP [] /negbTE ->.
Qed.

Lemma sieve_eject l1 n l2 :
  (forall x, x \in l1 -> x != n) ->
  (exists x, x \in l1 /\ x %| n) ->
  n \notin sieve (l1 ++ l2).
Proof.
elim: l1 => [| a l1 IHl1] //=; first by move=> _ [k [A _]].
move=> all_diff [] x; rewrite inE => [[]] /orP [/eqP xa | xin] xdivn.
  rewrite inE negb_or eq_sym all_diff ?inE ?eqxx // andTb.
  by rewrite mem_filter negb_and negbK -xa xdivn.
rewrite inE negb_or eq_sym all_diff ?inE ?eqxx // andTb.
rewrite mem_filter negb_and; apply/orP; right; apply: IHl1.
  by move=> y yin; apply: all_diff; rewrite inE yin orbT.
by exists x.
Qed.

Lemma sieve_prime n k :
  prime k -> k <= n.+1 -> k \in sieve (iota 2 n).
Proof.
move=> prk klen.
have kgt1 : 1 < k by move/primeP: prk => [].
have -> : n = ((k - 2) + (n - (k - 2))).
  rewrite addnC subnK // -ltnS (leq_trans _ klen) //.
  by move: kgt1; case: (k) => //= k'; rewrite subSS !ltnS !leq_subr.
rewrite iota_add.
have nk2gt0 : 0 < n - (k - 2).
   rewrite subn_gt0 -ltnS (leq_trans _ klen) //.
   by move: kgt1; case (k) => //= [][ | k'] //; rewrite !subSS subn0 leqnn.
rewrite -(prednK nk2gt0) /=  addnC subnK //.
apply: sieve_keep => //.
by apply/allP=> x; rewrite mem_iota /= => /andP [] ->.
Qed.

Lemma sieve_not_prime n k :
  1 < k <= n.+1 -> ~prime k -> k \notin sieve (iota 2 n).
Proof.
move=> /andP [] kgt1 klen /negP not_prime_k.
move/primePn: not_prime_k => [ | []x /andP [] xgt1 xltk xdivk].
  by rewrite ltnNge kgt1.
have -> :  n = (x - 1) + (n - (x - 1)).
  rewrite addnC subnK // -ltnS (leq_trans _ klen) // (leq_ltn_trans _ xltk) //.
  by rewrite leq_subr.
have xgt0 : 0 < x by apply: ltn_trans xgt1.
rewrite iota_add; apply: sieve_eject.
  move=> y; rewrite mem_iota addSn ltnS addnC subnK // => /andP [ygt1 yltx].
  have : y < k by apply: leq_ltn_trans xltk.
  by rewrite ltn_neqAle => /andP [].
by exists x; rewrite mem_iota xdivk addSn ltnS addnC subnK // leqnn xgt1.
Qed.

Lemma sieve_eq n k : 1 < k <= n.+1 ->
  prime k = (k \in sieve (iota 2 n)).
Proof.
move=> kin.
have [/sieve_prime -> //|/negP/(sieve_not_prime kin)/negbTE //] :=
  boolP (prime k).
by case/andP: kin.
Qed.

Fixpoint sieve3 (primes inputs : seq nat) :=
match inputs with
  x :: l' =>
  if has [pred p | p %| x] primes then
     sieve3 primes l'
  else
    sieve3 (x:: primes) l'
| nil => rev primes
end.

Time Compute sieve3 [::] (iota 2 2000).

Fixpoint has_divisor x (primes : seq nat) :=
  match primes with
    nil => false
  | p :: ps =>
    if p * p < x then
      if p %| x then true else has_divisor x ps
    else
      false
  end.
       
Fixpoint sieve4 (primes inputs : seq nat) :=
match inputs with
  x :: l' =>
  if has_divisor x primes then
    sieve4 primes l'
  else
    sieve4 (rcons primes x) l'
| nil => primes
end.

Time Compute sieve4 [::] (iota 2 2000).

Lemma sieve3_prime_keep l prs k:
  k \in prs -> k \in sieve3 prs l.
Proof.
elim: l prs => [ | a l IH].
  rewrite /=.
  by move=> prs; rewrite mem_rev.
rewrite /=.
move=> prs kinp; case: ifP => _; apply: IH => //.
by rewrite inE kinp orbT.
Qed.

Lemma mem_sieve3 l prs k :
  k \in sieve3 prs l -> (k \in l) || (k \in prs).
Proof.
elim: l prs => [ | a l IH] prs /=.
  by rewrite mem_rev.
case : (has [pred p | p %| a] prs).
  by move/IH => /orP [kin | kin]; rewrite ?inE kin orbT.
move/IH => /orP [|]; rewrite ?inE.
  by move=> ->; rewrite orbT.
by move=> /orP [] ->; rewrite ?orbT.
Qed.

Lemma sieve3_prime k prs l :
  all (ltn 1) prs -> all (ltn 1) l ->
  prime k -> k \in l -> k \in sieve3 prs l.
Proof.
elim: l prs => [ | a l IH] prs apgt1 algt1 pk kinl.
  by [].
rewrite /=.
case: ifP.
  move/hasP => [d dinprs] /= dda.
  move: kinl; rewrite inE => /orP[/eqP ka | kinl].
    move: apgt1 => /allP /(_ _ dinprs); rewrite /ltn /=.
    case/primeP: pk => kgt1 /(_ d); rewrite ka dda => /(_ isT) /orP[]/eqP.
      by move=> ->.
    by move=> da dgt1; apply: sieve3_prime_keep; rewrite -da.
  apply: IH => //.
  move/allP: algt1 => uni; apply/allP=> x xin; apply: uni.
  by rewrite inE xin orbT.
move=> _; move: kinl; rewrite inE => /orP[ka| kinl].
  by apply: sieve3_prime_keep; rewrite inE ka.
apply: IH => //.
  apply/allP=> x; rewrite inE => /orP [/eqP -> | ].
    by apply: (allP algt1 a); rewrite inE eqxx.
  by apply: (allP apgt1).
by apply/allP=> x xin; apply: (allP algt1); rewrite inE xin orbT.
Qed.
Lemma sieve3_eject k prs l1 l2 :
  k \notin prs ->
  has [pred p | p %| k] prs -> k \notin sieve3 prs l.
Proof.
elim: l prs => [/= | a l1 IH] prs.
  by rewrite mem_rev.
move=> /= knprs kdivided; case: ifP.
  by move=> adivided; apply: IH.
move=> anotdivided; apply: IH.
  rewrite inE; apply/negP=> /orP [/eqP ka | kinp].
    by move: kdivided; rewrite ka anotdivided.
  by move: knprs; rewrite kinp.
move/hasP: kdivided => [d din ddk].
by apply/hasP; exists d; rewrite ?ddk ?inE ?din ?orbT.
Qed.

(* This lemma is useless in its current shape. *)
Lemma sieve3_eject k prs l :
  k \notin prs ->
  has [pred p | p %| k] prs -> k \notin sieve3 prs l.
Proof.
elim: l prs => [/= | a l1 IH] prs.
  by rewrite mem_rev.
move=> /= knprs kdivided; case: ifP.
  by move=> adivided; apply: IH.
move=> anotdivided; apply: IH.
  rewrite inE; apply/negP=> /orP [/eqP ka | kinp].
    by move: kdivided; rewrite ka anotdivided.
  by move: knprs; rewrite kinp.
move/hasP: kdivided => [d din ddk].
by apply/hasP; exists d; rewrite ?ddk ?inE ?din ?orbT.
Qed.

Lemma sieve3_eq n x :
   (prime x && (x <= n.+1)) = (x \in sieve3 [::] (iota 2 n)).
Proof.
have [px | ] := boolP (prime x) => /=.
  have [xlen1 | igt ] := boolP (x <= n.+1).
    rewrite (sieve3_prime _ _ px) => //.
      by apply/allP=> y; rewrite mem_iota /ltn /= => /andP [] ->.
    by rewrite mem_iota add2n ltnS xlen1 andbT; move/primeP: px => [].
  apply/esym/negP => /mem_sieve3.
  by rewrite orbF mem_iota add2n ltnS (negbTE igt) andbF.
move=> nprx.