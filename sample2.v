Require Import Arith List.

Definition multiple_of div n :=
   exists k, n = k * div.

Definition prime n :=
  1 < n /\
  forall x, 1 < x < n -> ~ multiple_of x n.

Check forall (A : Type) (f : A -> bool) (x : A) (l : list A),
  In x (filter f l) <-> In x l /\ f x = true.

Lemma rewrite_exercise : forall (f g : nat -> nat),
  (forall y, f (f (f (f (g y)))) = 0) /\
  (f 0 = 0 \/ (forall x, f (f (g x)) = f (f (f (g x)))))
 ->
  forall z, f (f (f (f (f (g z)) ))) = 0.
Proof.
intros f g conditions z.
destruct conditions as [f4 [f0 | f2f3]].
  rewrite f4.
  rewrite f0.
  easy.
rewrite <- f2f3.
rewrite f4.
easy.
Qed.
