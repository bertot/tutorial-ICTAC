Require Import Arith ZArith Reals Interval.Interval_tactic List Psatz.
Require Znumtheory.

Definition remove_multiples x l :=
  filter (fun a => negb (a mod x =? 0)) l.

Fixpoint sieve l :=
  match l with
    a :: l' => a :: remove_multiples a (sieve l')
  | nil => nil
  end.

Compute seq 2 9.

Time Compute (sieve (seq 2 600)).

Open Scope Z_scope.

Definition Zremove_multiples x l :=
  filter (fun a => negb (a mod x =? 0)) l.

Fixpoint Zsieve l :=
  match l with
    a :: l' => a :: Zremove_multiples a (Zsieve l')
  | nil => nil
  end.

Fixpoint Zsieve_aux l bound :=
  match l with
    a :: l' => 
    if (bound <? a) then l else a :: Zremove_multiples a (Zsieve_aux l' bound)
  | nil => nil
  end.

Definition Zsieve2 l :=
  Zsieve_aux l (Z.sqrt (last l 2)).

Definition Znums n :=
  snd (iter (n-1) (Z * list Z)%type
            (fun p => let (a, l) := p in (a + 1,  (n - a) :: l)) (0, nil)).

Time Compute Zsieve2 (Znums 900).

Time Compute Zsieve (Znums 600).

Definition test n :=
  filter (fun p => negb (fst p =? snd p)) 
     (seq.zip (Zsieve (Znums n)) (Zsieve2 (Znums n))).

Compute test 800.

Close Scope Z_scope.

Lemma sieve_keep l1 n l2 :
  (forall x, In x l1 -> (n mod x <> 0)) ->
  In n (sieve (l1 ++ n :: l2)).
Proof.
induction l1 as [ | a l1 IHl1].
  simpl; left; auto.
simpl.
intros l1_condition.
right.
unfold remove_multiples.
rewrite filter_In.
split.
  apply IHl1.
  intros x x_in_l1.
  apply l1_condition.
  right.
  easy.
assert (n_mod_a_n0 : n mod a <> 0).
  apply l1_condition.
  now left; easy.
rewrite <- Nat.eqb_neq in n_mod_a_n0.
rewrite n_mod_a_n0.
easy.
Qed.

Lemma sieve_eject l1 n l2 :
(* At the time of making the proof, I discover the need for an extra
  condition *)
  (forall x, In x l1 -> x <> n) ->
  (exists x, In x l1 /\ n mod x = 0) ->
  ~In n (sieve (l1 ++ l2)).
Proof.
induction l1 as [ | a l1 IHl1].
  now intros _ [x [absurdity _]].
simpl; intros all_diff [x [[x_is_a | x_in_l1] n_mult_x]].
  intros [a_is_n | n_in_removed].
    assert (x_not_n := all_diff x).
    apply x_not_n; auto.
    now rewrite <- x_is_a.
  unfold remove_multiples in n_in_removed.
  rewrite filter_In in n_in_removed.
  rewrite x_is_a in n_in_removed.
  rewrite n_mult_x in n_in_removed.
  destruct n_in_removed.
  easy.
intros [a_is_n | n_in_removed].
  case (all_diff n).
    now left.
  easy.
revert n_in_removed.
unfold remove_multiples.
rewrite filter_In.
intros [n_in_removed nmoda].
case IHl1; auto.
now exists x.
Qed.

(* Now I need to prove the conditions for the list on which sieve works. *)

Lemma seq_dec len start n :
   start <= n < start + len ->
   seq start len = seq start (n - start) ++ seq n (len - (n - start)).
Proof.
revert start n; induction len as [ | len IH]; intros start n.
  rewrite Nat.add_0_r; intros abs.
  case (Nat.lt_irrefl start).
  now apply (Nat.le_lt_trans _ n); destruct abs.
intros [start_le_n n_lt_start_Slen].
replace (seq start (S len)) with (start :: (seq (S start) len)) by easy.
case (Nat.eq_dec n start).
  intros nstart; rewrite nstart.
  now rewrite Nat.sub_diag, Nat.sub_0_r.
intros n_n_start.
assert (nmstart : n - start = S (n - S start)) by lia.
rewrite nmstart.
simpl.
apply f_equal.
apply IH.
lia.
Qed.


Definition multiple_of div n :=
   exists k, n = k * div.

Definition prime n :=
  1 < n /\
  forall x, 1 < x < n -> ~ multiple_of x n.

Lemma sieve_prime n k :
  prime k -> k <= n ->
  In k (sieve (seq 2 (n - 1))).
Proof.
intros [kge2 no_div] k_le_n.
enough (seq_dec1 : seq 2 (n - 1) =
         seq 2 (k - 2) ++ k :: seq (k + 1) (n - 2 - (k - 2))).
  rewrite seq_dec1.
  apply sieve_keep.
  intros x Inx k_modx_0.
  assert (2 <= x < k) as [xge2 xltk].
    replace k with (2 + (k - 2)).
      now rewrite <- in_seq.
    now rewrite Nat.add_comm, Nat.sub_add.
  assert (xdivk : k = x * (k / x)).
    rewrite <- Nat.div_exact in k_modx_0; auto.
    lia.
  destruct (no_div x).
    lia.
  now exists (k / x); rewrite Nat.mul_comm.
rewrite (seq_dec _ _ k); cycle 1.
  split;[auto | lia].
replace (n - 1 - (k - 2)) with (S (n - 2 - (k - 2))) by lia.
simpl.
now replace (S k) with (k + 1) by lia.
Qed.

Lemma not_prime_divisor n :
  2 <= n -> ~prime n -> exists k, 1 < k < n /\ multiple_of k n.
Proof.
intros nge2 notprime.
assert (~Znumtheory.prime (Z.of_nat n)).
  intros HZp; destruct HZp as [nZge2 Zno_div].
  case notprime; split.
    now rewrite Nat2Z.inj_lt.
  intros x [xgt1 xltn] [k nxk].
  assert (intZ : (1 <= Z.of_nat x < Z.of_nat n)%Z).
    now rewrite <- (Nat2Z.inj_le 1), <- Nat2Z.inj_lt; lia.
  assert (relp := Zno_div _ intZ).
  rewrite nxk, Nat2Z.inj_mul in relp.
  destruct relp as [_ _ relp].
  assert (A := Z.divide_1_r_nonneg _ (Nat2Z.is_nonneg _)
     (relp (Z.of_nat x) (Z.divide_refl _)
     (Z.divide_mul_r _ _ _ (Z.divide_refl _)))).
  now revert xgt1; rewrite Nat2Z.inj_lt, A.
assert (nZgt1 : (1 < Z.of_nat n)%Z) by lia.
destruct (Znumtheory.not_prime_divide _ nZgt1 H) as [z [zg zd]].
assert (Zgt1 : 1 < Z.to_nat z) by (rewrite <- (Z2Nat.inj_lt 1); lia).
assert (Zlt1 : Z.to_nat z < n).
  now rewrite <- (Nat2Z.id n), <- Z2Nat.inj_lt; lia.
exists (Z.to_nat z);split;[lia | ].
destruct zd as [w pw]; exists (Z.to_nat w).
rewrite <- Z2Nat.inj_mul; try lia.
  now rewrite <- (Nat2Z.id n); apply f_equal.
assert (zgt0 : (0 < z)%Z) by lia.
assert (tmp := Z.mul_nonneg_cancel_l z w zgt0); revert tmp.
rewrite Z.mul_comm, <- pw => B; rewrite <- B.
apply Nat2Z.is_nonneg.
Qed.

Lemma sieve_not_prime n k :
  2 <= k -> ~prime k -> k <= n ->
  ~In k (sieve (seq 2 (n - 1))).
Proof.
intros kge2 not_prime_k klen.
destruct (not_prime_divisor _ kge2 not_prime_k) as [w [intw pw]].
assert (intw' : 2 <= w + 1 < 2 + (n - 1)) by lia.
rewrite (seq_dec _ _ _ intw').
replace (n - 1 - (k - 2)) with (S (n - 2 - (k - 2))) by lia.
simpl.
apply sieve_eject; cycle 1.
  exists w.
  rewrite in_seq; split; [ lia | ].
  now destruct pw as [w' pw']; rewrite pw', Nat.mod_mul; lia.
intros x; rewrite in_seq; intros; lia.
Qed.
